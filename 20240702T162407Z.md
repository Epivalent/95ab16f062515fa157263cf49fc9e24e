When the first argument to the `kill()` syscall is 0, it sends the signal to all processes in the same process group as the calling process. The process group ID (PGID) of a process can be obtained using the `getpgid()` syscall.

In other words, when `kill(0, sig)` is called, it sends the signal specified by `sig` to all processes in the same process group as the caller. This can be useful for broadcasting a signal to multiple related processes or for managing groups of processes together.
# Tue  2 Jul 16:24:07 CEST 2024 - what happens when the first argument to the kill() syscall is 0?